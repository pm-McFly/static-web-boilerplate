module.exports = {
  env: {
    browser: true,
    es6: true
  },
  extends: [
    "airbnb-base",
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:import/warnings"
  ],
  parserOptions: {
    ecmaFeatures: {
      experimentalObjectRestSpread: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  rules: {
    quotes: ["error", "double", { avoidEscape: true, allowTemplateLiterals: true }],
    "no-console": 1,
    "arrow-parens": ["error", "always"],
    "comma-dangle": ["error", "never"]
  }
};
